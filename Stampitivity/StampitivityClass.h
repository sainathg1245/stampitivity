//
//  StampitivityClass.h
//  Stampitivity
//
//  Created by BHANU on 25/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "SDWebImageManager.h"
#import "SDWebImageCompat.h"
@interface StampitivityClass : PFObject<PFSubclassing>
@property NSString *stampName;
@property PFFile *stampImage;
@property NSInteger stampPrice;
@property NSString * stampSubCategoty;
+(NSString *)parseClassName;

@end
