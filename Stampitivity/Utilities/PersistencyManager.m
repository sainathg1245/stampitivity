//
//  PersistencyManager.m
//  facebook
//
//  Created by Sai Nath Gourishetty on 9/24/14.
//  Copyright (c) 2014 PS pvt ltd. All rights reserved.
//

#import "PersistencyManager.h"

#define PROFILE_DB @"profile.db"

#define NAME @"name"
#define EMAIL @"email"
#define GENDER @"gender"
#define AGE @"age"
#define PROFILE_PIC_PATH @"profile_pic_path"

@implementation PersistencyManager

-(BOOL)checkAndCreateProfileDB{
    @try {
        NSString *docsDir;
        NSArray *dirPaths;
        
        // Get the documents directory
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        docsDir = [dirPaths objectAtIndex:0];
        
        // Build the path to the database file
        profileDBPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: PROFILE_DB]];
        
        NSFileManager *filemgr = [NSFileManager defaultManager];
        
        if ([filemgr fileExistsAtPath:profileDBPath] == NO)
        {
            const char *dbpath = [profileDBPath UTF8String];
            if (sqlite3_open(dbpath, &profile_database) == SQLITE_OK)
            {
                char *errMsg;
                NSString *createSQL = [NSString stringWithFormat: @"CREATE TABLE IF NOT EXISTS ProfileTable (id integer PRIMARY KEY, %@ text, %@ text,%@ text, %@ text,%@ text)",NAME,EMAIL,GENDER,AGE,PROFILE_PIC_PATH];
                const char *sql_stmt = [createSQL UTF8String];
                
                if (sqlite3_exec(profile_database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    NSLog(@"profile_database and profile_database created failed");
                    sqlite3_close(profile_database);
                    return YES;
                }
                else
                {
                    NSLog(@"profile_database created successfully");
                    sqlite3_close(profile_database);
                    return YES;
                }
                
            } else {
                return NO;
                NSLog(@"profile_database created failed");
            }
        }
        return YES;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in checkAndCreateProfileDB. Exception name : %@, Description : %@",[exception name],[exception description]);    }
}

-(void)insertProfileData:(Profile*)profData{
    @try {
        sqlite3_stmt    *statement;
        @synchronized(self)
        {
            if ([self checkAndCreateProfileDB])
            {
                if (sqlite3_open([profileDBPath UTF8String], &profile_database)==SQLITE_OK)
                {
                    NSString *name = [profData name];
                    NSString *email = [profData email];
                 //   NSString *gender = [profData gender];
                    NSString *age = [profData age];
                    NSString *profPicPath = [profData profilePicPath];
                    NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO ProfileTable (%@,%@,%@,%@) VALUES ('%@','%@','%@','%@')",NAME,EMAIL,AGE,PROFILE_PIC_PATH,name,email,age,profPicPath];
                    const char *insert_stmt = [insertSQL UTF8String];
                    if(sqlite3_prepare_v2(profile_database, insert_stmt, -1, &statement, NULL) == SQLITE_OK)
                    {
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            NSLog(@"insert successfully ProfileTable  ");
                        } else {
                            NSLog(@"Inserting  ProfileTable Record  falied");
                        }
                        sqlite3_finalize(statement);
                    }
                    sqlite3_close(profile_database);
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in insertion profile data. Exception name : %@, Description : %@",[exception name],[exception description]);
    }
}

-(Profile*)getProfileData{
    Profile *profData;
    sqlite3_stmt    *statement;
    @try {
        @synchronized(self) {
            if ([self checkAndCreateProfileDB])
            {
                if(sqlite3_open([profileDBPath UTF8String], &profile_database)==SQLITE_OK)
                {
                    NSString* querySQL = [NSString stringWithFormat: @"SELECT  %@, %@,%@,%@,%@   FROM ProfileTable ORDER BY id DESC LIMIT 1",NAME,EMAIL,GENDER,AGE,PROFILE_PIC_PATH];
                    
                    const char * query_stmt = [querySQL UTF8String];
                    
                    if (sqlite3_prepare_v2(profile_database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
                    {
                        if (sqlite3_step(statement) == SQLITE_ROW)
                        {
                            do {
                                profData = [[Profile alloc]init];
                              NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                              NSString *email = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
//                              NSString *gender = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                               NSString *age = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                                NSString *profPicPath = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];

                                [profData setName:name];
                                [profData setEmail:email];
                              //  [profData setGender:gender];
                                [profData setAge:age];
                                [profData setProfilePicPath:profPicPath];
                            }  while (sqlite3_step(statement) == SQLITE_ROW);
                            sqlite3_finalize(statement);
                            sqlite3_close(profile_database);
                            return profData;
                        }  else {
                            
                            sqlite3_finalize(statement);
                            sqlite3_close(profile_database);
                            return nil;
                        }
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in deleting profile data. Exception name : %@, Description : %@",[exception name],[exception description]);
    }
    
    return profData;
}

-(void)updateProfileData:(Profile*)profData{
    @try {
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in updating profile data. Exception name : %@, Description : %@",[exception name],[exception description]);
    }
}
-(void)deleteProfileData:(Profile*)profData{
    @try {
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in deleting profile data. Exception name : %@, Description : %@",[exception name],[exception description]);
    }
}

@end
