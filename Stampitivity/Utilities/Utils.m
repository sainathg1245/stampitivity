
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <netdb.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@implementation Utils

+(BOOL) validateEmail:(NSString *) email
{
    @try {
        NSString *emailRegEx =
        @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
        @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
        @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
        @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
        @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
        @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
        @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        
        NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        return [regExPredicate evaluateWithObject:email];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in validateEmail method of Util: %@",[exception name]);
        return NO;
    }
}

+(BOOL) validatePassword:(NSString *)password
{
    @try{
        NSString *pwdRegEx = @".*[0-9].*";
        
        NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pwdRegEx];
        return [regExPredicate evaluateWithObject:password];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in validatePassword method of Util: %@",[exception name]);
        return NO;
    }
    
}

+(double) getCurrentTimeinMillis
{
    @try{
        //NSLog(@"%f",[[NSDate date] timeIntervalSince1970]);
        double f =  [[NSString stringWithFormat:@"%0.f",[[NSDate date] timeIntervalSince1970] * 1000 ] doubleValue];
        //NSLog(@"***%f",f);
        return  f;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in getCurrentTimeinMillis method of Util: %@",[exception name]);
        return 0;
    }
}


+ (BOOL) connectedToNetwork
{
    @try {
        // Create zero addy
        struct sockaddr_in zeroAddress;
        bzero(&zeroAddress, sizeof(zeroAddress));
        zeroAddress.sin_len = sizeof(zeroAddress);
        zeroAddress.sin_family = AF_INET;
        
        // Recover reachability flags
        SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
        SCNetworkReachabilityFlags flags;
        
        BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
        CFRelease(defaultRouteReachability);
        
        if (!didRetrieveFlags)
        {
            return NO;
        }
        
        BOOL isReachable = flags & kSCNetworkFlagsReachable;
        BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
        return (isReachable && !needsConnection) ? YES : NO;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in connectedToNetwork method of Util: %@",[exception name]);
        return NO;
    }
}
@end
