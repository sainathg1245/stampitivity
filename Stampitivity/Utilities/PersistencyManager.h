//
//  PersistencyManager.h
//  facebook
//
//  Created by Sai Nath Gourishetty on 9/24/14.
//  Copyright (c) 2014 PS pvt ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<sqlite3.h>
#import "Profile.h"

@interface PersistencyManager : NSObject
{
    sqlite3 *profile_database;
    NSString *profileDBPath;
}

-(void)insertProfileData:(Profile*)profData;
-(Profile*)getProfileData;
-(void)updateProfileData:(Profile*)profData;
-(void)deleteProfileData:(Profile*)profData;

@end
