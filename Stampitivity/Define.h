//
//  Define.h
//  Piczly
//
//  Created by BHANU on 16/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#define Com_AlertView(TITLE, MSG) [[[UIAlertView alloc] initWithTitle:(TITLE) message:(MSG) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]


#define kUserPlaceholderIconImage [UIImage imageNamed:@"user_icon"]


typedef enum _ITEAM_TYPE
{
    IT_PRODUCTS                 = 0,
    IT_SERVICE                  = 1,
    IT_MY_STUFF                 = 2,
//    FindFriends                 = 3,
} ITEAM_TYPE;


// Parse Class Names
//#define CLASS_Inventory              @"Inventory"
//#define CLASS_User                   @"User"
//#define CLASS_Cart                   @"Inventory"
//#define CLASS_Categories             @"Categories"
//#define CLASS_Ratings                @"Ratings"
//#define CLASS_Recommendations        @"Recommendations"
//#define CLASS_Sales                  @"Sales"
//#define CLASS_Transactions           @"Transactions"
//#define InternetConnection           @"please check your internet connection"


//#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "StampitivityUser.h"
#import "Profile.h"
#import "Utils.h"
//#endif
