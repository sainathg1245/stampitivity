//
//  ViewController.h
//  Stampitivity
//
//  Created by BHANU on 17/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
- (IBAction)btnActionForFacebook:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *textfForEmail;
@property (strong, nonatomic) IBOutlet UITextField *textfForPassword;

- (IBAction)btnActionForSignin:(id)sender;


- (IBAction)btnActionforSignup:(id)sender;
@property BOOL isRemember;
@property (nonatomic, strong) UILabel *errorMessageLabel;
- (IBAction)btnActionForgotPassword:(UIButton *)sender;

@end

