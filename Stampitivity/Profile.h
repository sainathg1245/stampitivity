//
//  Profile.h
//  Piczly
//
//  Created by BHANU on 16/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject
@property (nonatomic, strong) NSString *name, *email,*gender ,*age,*profilePicPath;
@end
