//
//  RegisterViewController.m
//  Stampitivity
//
//  Created by BHANU on 19/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import "RegisterViewController.h"
#import "ViewController.h"
#import "Define.h"
@interface RegisterViewController ()

@end
ViewController *VC;
@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.textfForEmail.delegate = self;
    self.textfForName.delegate = self;
    self.textfForPassword.delegate = self;
    self.textfForConfirmPassword .delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnActionForCancel:(UIButton *)sender
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnActionForRegister:(UIButton *)sender
{
    NSString *email = [_textfForEmail.text lowercaseString];
    NSString *name=[_textfForName.text capitalizedString];
    NSString *password = _textfForPassword.text;
    // NSString *dob=dobTxt.text;
    // NSString *gender=genderTxt.text;
    
    
    if(name==nil || name.length == 0)
    {
        [self showAlert:@"Please enter name." Tittle:@"Missing Profile Information"];
        return;
    }
    else if(email==nil || email.length < 1) {
        [self showAlert:@"Please enter email address." Tittle:@"Missing Profile Information"];
        return;
    }
    else if([Utils validateEmail:email] == NO)
    {
        [self showAlert:@"Please enter valid email address." Tittle:@"Missing Profile Information"];
        return;
    }
    else if(password==nil || password.length == 0)
    {
        [self showAlert:@"Please enter password." Tittle:@"Missing Profile Information"];
        return;
    }
    else if (![password isEqualToString:self.textfForConfirmPassword.text])
    {
        [self showAlert:@"Alert!" Tittle:@"Passwords do not match!!"];
        return;
    }
    [self validateUser];

}


-(void) showAlert:(NSString *) msg Tittle:(NSString*)title{
    UIAlertView * errAlert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [errAlert show];
}

- (void)validateUser
{
    PFQuery *queryForUser=[StampitivityUser query];
    //PFQuery *queryForUser = [PFQuery queryWithClassName:@"User"];
    [queryForUser whereKey:@"email" equalTo:_textfForEmail.text];
    [queryForUser whereKey:@"username" equalTo:_textfForName.text];
    NSLog(@"%@ uesrname",_textfForName.text);
    // [queryForUser whereKey:@"username" equalTo:[_nametxtf.text substringFromIndex:[_nametxtf.text length]-5]];
    
    // [queryForUser getObjectInBackgroundWithId:@"username"];
    // [queryForUser get]
    
    //[queryForUser whereKey:@"username" equalTo:_nametxtf.text];
    [queryForUser getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (object)
        {
            //            [object objectForKey:@"username"];
            //            NSLog(@"");
            Com_AlertView(@"Info", @"Oops! You're already registered. Forget your username?");
            //  [MBProgressHUD hideHUDForView:self.view animated:YES];
            return ;
        }
        else
        {
            [self registeredUser];
            
        }
    }];
}

- (void)registeredUser
{
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *s = [NSMutableString stringWithCapacity:5];
    for (NSUInteger i = 0U; i < 5; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    
    NSLog(@"%@",s);
    _currentUser = [PFUser user];
    _currentUser[@"userType"] = [NSNumber numberWithInt:1];
    //_currentUser.username =  [_nametxtf.text capitalizedString];
    _currentUser.username = [_textfForName.text stringByAppendingString:s];
    _currentUser.email = [_textfForEmail.text lowercaseString];
    _currentUser.password = _textfForPassword.text;
    //_currentUser [@"name"] = [_nametxtf.text capitalizedString];
    
    
    
    
    //    LoginUser *user = (LoginUser *)[PFUser currentUser];
    //    [user setObject:[_nametxtf.text capitalizedString] forKey:@"name"];
    //    [user setObject:[_emailTxtf.text lowercaseString] forKey:@"email"];
    //    [user setObject:_passwordTxtf.text forKey:@"password"];
    
    //    PFObject  *obj = [PFObject objectWithClassName:@"User"];
    //    [obj setObject:[_nametxtf.text capitalizedString] forKey:@"name"];
    //    [obj setObject:[_emailTxtf.text lowercaseString] forKey:@"email"];
    //    [obj setObject:_passwordTxtf.text forKey:@"password"];
    //   // [obj saveInBackground];
    //    [obj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
    //        if (error)
    //        {
    //            NSLog(@"%@ error msg",error);
    //        }
    //        else{
    //            NSLog(@"%@ no error",error);
    //        }
    //
    //    }];
    
    
    
    [_currentUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            
            [_activityIndicator setHidesWhenStopped:YES];
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_textfForEmail.text] forKey:@"username"];
            //[[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_emailTxtf.text] forKey:@"name"];
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_textfForPassword.text] forKey:@"pwd"];
            
            [self showAlert:@"Registered Successfully" Tittle:@"Status"];
            ViewController  *VCLogin  = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            [self presentViewController:VCLogin animated:YES completion:nil];
            
        }
        else  {
            switch ([error code])
            {
                case 100:
                {
                    _validationAlert=[[UIAlertView alloc]initWithTitle:@"ConnectionFailed" message:@"Please check your Internet Connectivity" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [_validationAlert show];
                    //           [MBProgressHUD hideHUDForView:self.view animated:YES];
                    break;
                }
                case 202:
                {
                    _validationAlert=[[UIAlertView alloc]initWithTitle:@"Connection Failed" message:@"email or username has already been taken" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [_validationAlert show];
                    //     [MBProgressHUD hideHUDForView:self.view animated:YES];
                    break;
                }
                default:
                    //                    _validationAlert=[[UIAlertView alloc]initWithTitle:@"ConnectionFailed" message:@"Unknown issue" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //                    [_validationAlert show];
                    break;
            }
            _activityIndicator.hidden=YES;
        };
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
@end
