//
//  StampitivityPayVC.h
//  Stampitivity
//
//  Created by BHANU on 26/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StampitivityPayVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *StampImageDisplay;
@property (strong, nonatomic) IBOutlet UIButton *btnPayAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblStampCategory;

@property NSString *stampSubCategoryType;
@property NSInteger *stampPrice;

@end
