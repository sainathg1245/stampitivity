//
//  DataVCCell.h
//  Stampitivity
//
//  Created by BHANU on 24/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataVCCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UILabel *lblCategoryName;
@property (strong, nonatomic) IBOutlet UILabel *lblStampPrice;

@end
