//
//  DataViewController.m
//  Stampitivity
//
//  Created by BHANU on 19/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import "DataViewController.h"
#import "DataVCCell.h"
#import <Parse/Parse.h>
#import "CategoryClass.h"
#import "StampitivityClass.h"
#import "UIImageView+WebCache.h"

@interface DataViewController ()
{
    NSMutableArray *arrayForStampitivityName;
     NSMutableArray *arrayForStampitivityImage;
    NSMutableArray *arrayForStampPrice;
    StampitivityClass *objClass;
}
@end

@implementation DataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayForStampitivityName=[[NSMutableArray alloc]init];
    arrayForStampitivityImage=[[NSMutableArray alloc]init];
    arrayForStampPrice=[[NSMutableArray alloc]init];
    
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}


-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
 return arrayForStampitivityName.count;
    
}

-(void)getarray
{
    PFQuery *getObj = [PFQuery queryWithClassName:@"categories"];
    [getObj findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (objects>0) {
            for(int i=0;i<objects.count;i++)
            {
                objClass=[StampitivityClass object];
              //  [objClass setStampSubCategoty:[[objects objectAtIndex:i]valueForKey:@"categoryType" ]];
                [objClass setObject:[objects objectAtIndex:i] forKey:@"categoryID"];
               // [objClass setImageFile:[[objects valueForKey:@"image"]objectAtIndex:0]];
               // [categoriesName addObject: [[objects valueForKey:@"categoriesType"]objectAtIndex:i]];
                [objClass saveInBackground];
            }
                  [self.tableView reloadData];
        }
        
    }];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier=@"ReloadData";
    DataVCCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
        
    {
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"DataVCCell" owner:self options:nil];
        
        cell=(DataVCCell*)[nib objectAtIndex:0];
        
    }
    
    
    StampitivityClass *objStampData=[arrayForStampitivityName objectAtIndex:indexPath.row];
    
    cell.lblCategoryName.text=objStampData.stampName;
    cell.lblStampPrice.text=   [NSString stringWithFormat:@"$%li",(long)objStampData.stampPrice];
   
    
    if (objStampData.stampImage.url.length>0) {
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = cell.imageView.center;
        activityIndicator.hidesWhenStopped = YES;
        [cell addSubview:activityIndicator];
        [activityIndicator startAnimating];
        
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:objStampData.stampImage.url] placeholderImage:[UIImage imageNamed:@"vdo_preview.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            [activityIndicator stopAnimating];
            [activityIndicator removeFromSuperview];
        }];
    }
    else
    {
        cell.imageView.image=[UIImage imageNamed:@"PlaceHolder.png"];
    }

   // cell.lblStampPrice.text=
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 78;
}

-(IBAction)mthReload:(id)sender
{
       [self getarray];
}
-(IBAction)mthStampData:(id)sender
{
    [arrayForStampitivityName removeAllObjects];
    [self mthStampitivityData];
    
}
-(void)mthStampitivityData
{
    
    PFQuery *getObj = [PFQuery queryWithClassName:@"StampitivityItems"];
    [getObj findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
        
        if (objects.count>0) {
            [arrayForStampitivityName addObjectsFromArray:objects];
            [self.tableView reloadData];
        }
        }
//        for (int i=0; i<objects.count; i++) {
//            
//            PFFile *ThumbImage=[[objects valueForKey:@"stampImage"] objectAtIndex:i];
//           
//            
//            
//        }
        
//        if (objects>0) {
//            for (int i=0; i<objects.count; i++)
//            {
//                if ([[objects objectAtIndex:0]valueForKey:@"stampImage"]) {
//                    PFFile *eventImage = [[objects valueForKey:@"stampImage"]objectAtIndex:i];
//                        [eventImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//                            UIImage *thumbnailImage = [UIImage imageWithData:data];
//                            [arrayForStampitivityImage addObject:thumbnailImage];
//                            
//                        }];
//                }
//                
//                
//                [arrayForStampitivityName addObject:[[objects valueForKey:@"stampName"]objectAtIndex:i]];
//                [arrayForStampPrice addObject:[[objects valueForKey:@"stampPrice"]objectAtIndex:i]];
//            
//            }
//            
//            //            for(int i=0;i<objects.count;i++)
//            //            {
//            //                objClass=[CategoryClass object];
//            //                objClass[@"categoriesName"]=[[objects objectAtIndex:i]valueForKey:@"categoriesType" ];
//           // cell.lblCategoryName.text=[[objects valueForKey:@"categoriesType"]objectAtIndex:indexPath.row];
//            //                cell.imageView.image=[[objects valueForKey:@"Image"]objectAtIndex:indexPath.row];
//            //            }
//            //[self.tableView reloadData];
//        }
        
    }];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StampitivityClass *objStampData=[arrayForStampitivityName objectAtIndex:indexPath.row];
    StampitivityPayVC *objTransferPayDetails=[[StampitivityPayVC alloc]init];
    objTransferPayDetails.stampSubCategoryType=objStampData.stampName;
    objTransferPayDetails.stampPrice= (NSInteger *)objStampData.stampPrice;
    
    
    if (objStampData.stampImage.url.length>0) {
        [objTransferPayDetails.StampImageDisplay sd_setImageWithURL:[NSURL URLWithString:objStampData.stampImage.url] placeholderImage:[UIImage imageNamed:@"vdo_preview.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            NSLog(@"Transfering the data to another view controller");
            
        }];
    }
    else
    {
    objTransferPayDetails.StampImageDisplay.image=[UIImage imageNamed:@"PlaceHolder.png"];
        
    }

    [self presentViewController:objTransferPayDetails animated:YES completion:nil];
    
 //  objTransferPayDetails.StampImageDisplay.image=objClass.stampImage;
    
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
