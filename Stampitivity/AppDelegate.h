//
//  AppDelegate.h
//  Stampitivity
//
//  Created by BHANU on 17/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Bolts/BFAppLinkResolving.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

