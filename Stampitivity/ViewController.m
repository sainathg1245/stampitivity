//
//  ViewController.m
//  Stampitivity
//
//  Created by BHANU on 17/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import "ViewController.h"

#import <FacebookSDK/FacebookSDK.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "Define.h"
#import "RegisterViewController.h"
#import "DataViewController.h"
@interface ViewController ()
{
    NSDictionary *userData;
}

@end
NSArray *permissionsArray;
UIActivityIndicatorView *activityIndicator;

@implementation ViewController
@synthesize errorMessageLabel;
NSString *facebookID;
NSString *name;
NSString *location;
NSString *gender;
NSString *birthday;
NSString *relationship;
NSURL *pictureURL;
NSString *email;
NSString *firstname;
NSString *lastName;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.textfForEmail.delegate =self;
    self.textfForPassword.delegate = self;

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnActionForFacebook:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location",@"email",@"user_friends"];
    
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if (!user) {
            NSString *errorMessage = nil;
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                errorMessage = @"Uh oh. The user cancelled the Facebook login.";
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                errorMessage = [error localizedDescription];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
        } else {
            if (user.isNew) {
                NSLog(@"User with facebook signed up and logged in!");
                
                [self loadData];
            } else
            {
                NSLog(@"User with facebook logged in!");
                [self loadData];
                //[self presentUserDetailsViewControllerAnimated];
                
                
            }
        }
    }];
    
    [activityIndicator startAnimating]; // Show loading indicator until login is finished
    

}



- (void)loadData {
    // ...
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [FBRequestConnection startWithGraphPath:@"me"
                                 parameters:[NSDictionary dictionaryWithObject:@"email,first_name,last_name,picture,gender,age_range,name" forKey:@"fields"]
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                  
                                  // result is a dictionary with the user's Facebook data
                                  userData = (NSDictionary *)result;
                                  __block StampitivityUser *user=[StampitivityUser currentUser];
                                  
                                  PFQuery *query=[StampitivityUser query];
                                  
                                  [query whereKey:@"email" equalTo:userData[@"email"]];
                                  [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
//                                      if(number>0){
//                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                                          // delete the user that was created as part of Parse's Facebook login
//                                          [user deleteInBackground];
//                                         // [user ]
//                                          [[FBSession activeSession] closeAndClearTokenInformation];
//                                          
//                                          //[self presentUserDetailsViewControllerAnimated];
//                                          //                        Com_AlertView(@"info",@" It looks like you already have an account");
//                                      }
//                                      else
                                      //{
                                          
                                          NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
                                          NSMutableString *s = [NSMutableString stringWithCapacity:5];
                                          for (NSUInteger i = 0U; i < 5; i++) {
                                              u_int32_t r = arc4random() % [alphabet length];
                                              unichar c = [alphabet characterAtIndex:r];
                                              [s appendFormat:@"%C", c];
                                          }
                                          
                                          NSLog(@"%@",s);
                                          
                                          
                                          user[@"facebookId"] = userData[@"id"];
                                      user[@"displayName"] = [userData[@"first_name"] stringByAppendingString:userData[@"last_name"]];
                                          //user[@"displayName"]=userData[@"first_name"];
                                          user[@"firstName"]= userData[@"first_name"];
                                          user[@"lastName"] =userData[@"last_name"];
                                          user[@"username"] = [userData[@"name"] stringByAppendingString:s];
                                          
                                          facebookID=userData[@"id"];
                                          name=userData[@"name"];
                                          user[@"email"]=userData[@"email"];
                                        user[@"userType"] = [NSNumber numberWithInt:2];
                                          
                                          
                                          
                                          NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", userData[@"id"]]];
                                          user[@"image"] = [NSString stringWithFormat:@"%@", pictureURL];
                                          
                                          //                                                  PFFile *userImage = [PFFile fileWithData:[NSData dataWithContentsOfURL:pictureURL]];
                                          //                                                  [NewUser currentUser].userImageFile = userImage;
                                          [[StampitivityUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                              [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                              if (!error)
                                              {
                                                  
                                                  // [AppDelegate.getInstance resetViewsAfterLogin];
                                                  
                                                  //                            FANSTOPHomeViewController *piczlyLoginVC=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
                                                  //                            piczlyLoginVC.profileName.text=user[@"username"];
                                                  //                            [self.navigationController pushViewController:piczlyLoginVC animated:YES];
                                                  
                                                  // [AppDelegate.getInstance resetViewsAfterLogin];
                                                  
                                                  
                                              }
                                              else
                                              {
                                                  Com_AlertView(@"Error", error.localizedDescription);
                                              }
                                              
                                          }];
                                     // }
                                  }];
                              }
                              else
                              {
                                  Com_AlertView(@"error", error.localizedDescription);
                              }
                          }];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    if ([PFUser currentUser] && // Check if user is cached
        [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) { // Check if user is linked to Facebook
        // Present the next view controller without animation
        //[self presentUserDetailsViewControllerAnimated];
    }
    
}


- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if (!error) {
                // We have a valid session
                //DLog(@"User session found");
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *FBuser, NSError *error) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    if (!error) {
                        
                        
                        PFQuery *querys = [StampitivityUser query];//[PFQuery queryWithClassName:@"User"];
                        [querys whereKey:@"email" equalTo:[FBuser objectForKey:@"email"]];
                        
                        NSArray *existingUserAry = [querys findObjects];
                        
                        if ([existingUserAry count]>0)
                        {
                           
                            
                        }
                        
                        else {
                            NSLog(@"hellooo ...");
                            StampitivityUser *user = [StampitivityUser object];
                            user.username = [FBuser objectForKey:@"email"];
                            user.password = [FBuser objectForKey:@"email"];
                            user.email  = [FBuser objectForKey:@"email"];
                            user[@"facebookId"]  = FBuser[@"id"];
                            user.displayName = FBuser[@"name"];
                            user.location = FBuser[@"location"][@"name"];
                            
                            //                            PFFile *userImage = [PFFile fileWithName:@"userImage" contentsAtPath:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=square",[FBuser objectForKey:@"id"]]];
                            //                            [BKKUser currentUser].userImageFile = userImage;
                            [[StampitivityUser currentUser] saveEventually];
                            
                            //                            [MNGSaveObject saveToUserDefaults:@"userNameKey" :[FBuser objectForKey:@"name"]];
                            //                            [MNGSaveObject saveToUserDefaults:@"emailKey" :[FBuser objectForKey:@"email"]];
                            //                            [MNGSaveObject saveToUserDefaults:@"imageIdKey" :[FBuser objectForKey:@"id"]];
                            
                            BOOL status = [user signUp];
                            NSLog(@"Status %d",status);
                            if(status)
                            {
                                //                                [AppDelegate.getInstance resetViewsAfterLogin];
                               // [self presentUserDetailsViewControllerAnimated];
                                
                            }
                            
                        }
                        
                    }
                }];
                
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
}

-(void)request:(FBRequest *)request didLoad:(id)result{
    NSLog(@"result is : %@", result);
}

- (IBAction)btnActionForSignin:(id)sender
{
    NSString *log_username = [_textfForEmail.text lowercaseString];
    NSString *log_password = _textfForPassword.text;
    if(log_username==nil || log_username.length < 1) {
        
        [self showAlert:@"Please enter email address." Tittle:@"Missing Profile Information"];
        return;
    }
    
    else if(log_password==nil || log_password.length == 0) {
        [self showAlert:@"Please enter password." Tittle:@"Missing Profile Information"];
        return;
    }
    else{
        NSString *newString = [_textfForEmail.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        _textfForEmail.text  = newString;
        [self loginUser];
        [self showAlert:@"successed" Tittle:@"Login Status"];
        DataViewController *DataVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DataVC"];
        [self presentViewController:DataVC animated:YES completion:nil];
        
    }
}

    
-(void) showAlert:(NSString *) msg Tittle:(NSString*)title{
        UIAlertView * errAlert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [errAlert show];
    }
    
- (IBAction)btnActionforSignup:(id)sender
{
    RegisterViewController *RegisterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Register"];
    [self presentViewController:RegisterVC animated:YES completion:nil];
}

- (void)loginUser
{
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    PFQuery *query = [StampitivityUser query];
    [query whereKey:@"email" equalTo:_textfForEmail.text];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if (objects.count > 0) {
            
            PFObject *object = [objects objectAtIndex:0];
            NSString *username = [object objectForKey:@"username"];
            [PFUser logInWithUsernameInBackground:username password:_textfForPassword.text block:^(PFUser* user, NSError* error){
                if (!error) {
                    StampitivityUser *user = [StampitivityUser currentUser];
                    Profile *profData = [[Profile alloc]init];
                    [profData setName:user.username];
                    [profData setEmail:user.email];
                    
                    [profData setGender:user.Gender];
                    [profData setProfilePicPath:nil];
                    
                    //   [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    
                    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                    [currentInstallation setObject:[PFUser currentUser].objectId forKey:@"userId"];
                    currentInstallation[@"currentuser"]=[PFUser currentUser];
                    [currentInstallation setObject:[PFUser currentUser] forKey:@"owner"];
                    [currentInstallation saveInBackground];
                    NSLog(@"currentInstallation from login : %@",currentInstallation);
                    if (_isRemember) {
                        [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"username"];
                        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_textfForPassword.text] forKey:@"pwd"];
                        
                    }
                    // [self moveToHomeView];
                }
                
                
                if (error) {
                    // [MBProgressHUD hideHUDForView:self.view animated:YES];
                    Com_AlertView(nil, @"Invalid Email/Password");
                    
                }
                
                
            }];
        }else{
            // [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            Com_AlertView(nil, @"The Email ID not found");
            
            
        }
        
        
    }];
}


- (IBAction)btnActionForgotPassword:(UIButton *)sender
{
    UIAlertView *emailAlert=[[UIAlertView alloc]initWithTitle:@"Enter your Email-id" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    emailAlert.alertViewStyle =UIAlertViewStylePlainTextInput;
    [emailAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
           // Com_AlertView(nil, @"user cancelled");
        return;
        
    }else if (buttonIndex == 1)
    {
        //NSString *name ;
        name = [alertView textFieldAtIndex:0].text;
        
    }
    if (![Utils validateEmail:name]) {
        Com_AlertView(nil, @"Invalid Email ID");
        return;
    }
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFQuery *querys = [StampitivityUser query];
    
    
    [querys whereKey:@"email" equalTo:name];
    [querys findObjectsInBackgroundWithBlock:^(NSArray *object, NSError *error) {
        
        if ([object count]>0 ) {
             if([[[object objectAtIndex:0] valueForKey:@"userType"]doubleValue] ==[[NSNumber numberWithInt:1]doubleValue])
            {
                [StampitivityUser requestPasswordResetForEmailInBackground:name block:^(BOOL succeeded,NSError *error)
                 {
                     
                     if (!error) {
                              [MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         Com_AlertView(nil, @"You're almost done. We have emailed you a link to reset your password.");
                         
                         return;
                         
                     }
                     else
                     {
                         NSString *errorString = [error userInfo][@"error"];
                         NSString *message = [NSString stringWithFormat: @"Password reset failed: %@",errorString];
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         Com_AlertView(nil,message );
                         return;
                     }
                 }];
                
            }else if([[[object objectAtIndex:0] valueForKey:@"userType"]doubleValue] ==[[NSNumber numberWithInt:2]doubleValue])
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                Com_AlertView(nil, @"You are a facebook registered user,you cannot reset the password using this");
                
                
                return;
                
            }

            
        }
    }];
}



#pragma mark - textfiled delegate methods

- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = 80;
    const float movementDuration = 0.3f;
    int movement = (up ? -movementDistance : movementDistance);
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (textField ==self.textfForEmail || self.textfForPassword)
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x,-180,
                                     self.view.frame.size.width, self.view.frame.size.height);
    }else
    {
        self.view .frame = CGRectMake(self.view.frame.origin.x,0,
                                      self.view.frame.size.width, self.view.frame.size.height);
    }
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (textField ==self.textfForEmail || self.textfForPassword)
    {
        self.view .frame = CGRectMake(self.view.frame.origin.x,0,
                                      self.view.frame.size.width, self.view.frame.size.height);
    }else
    {
        self.view .frame = CGRectMake(self.view.frame.origin.x,0,
                                      self.view.frame.size.width, self.view.frame.size.height);
        
    }
    
    [UIView commitAnimations];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return textField;
}

#pragma mark - PFLogInViewControllerDelegate

// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(ViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    
    // if guest login
    if ([[username lowercaseString] isEqualToString:@"guest"])
    {
        logInController.errorMessageLabel.text = @"Email/Pass inncorrect";
        return NO;
    }
    
    if (username && password && username.length && password.length) {
        NSString *emailIdentifier = @"@";
        if ([username rangeOfString:emailIdentifier].location != NSNotFound) {
            
            PFQuery *query = [PFUser query];
            [query whereKey:@"email" equalTo:username];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
                if (objects.count > 0)
                {
                    PFObject *object = objects[0];
                    NSString *username = object[@"username"];
                    
                    [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser* user, NSError* error)
                     {
                         // PFUser* fanStopUser = (PFUser*)user;
                         
                         if (error)
                         {
                             logInController.errorMessageLabel.text = @"Email/Pass inncorrect";
                         }
                         // [self presentUserDetailsViewControllerAnimated];
                     }];
                }
                else
                {
                    logInController.errorMessageLabel.text = @"Email/Pass inncorrect";
                }
            }];
            return NO;
        }
        
        return YES;
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO;
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


@end
