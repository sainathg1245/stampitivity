//
//  RegisterViewController.h
//  Stampitivity
//
//  Created by BHANU on 19/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
@interface RegisterViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *textfForName;
@property (strong, nonatomic) IBOutlet UITextField *textfForEmail;
@property (strong, nonatomic) IBOutlet UITextField *textfForPassword;

@property (strong, nonatomic) IBOutlet UITextField *textfForConfirmPassword;
- (IBAction)btnActionForCancel:(UIButton *)sender;

- (IBAction)btnActionForRegister:(UIButton *)sender;
@property PFUser *currentUser;
@property UIActivityIndicatorView *activityIndicator;
@property UIAlertView * validationAlert;
@end
