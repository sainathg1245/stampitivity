//
//  CategoryClass.h
//  Stampitivity
//
//  Created by BHANU on 24/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
@interface CategoryClass : PFObject<PFSubclassing>

@property NSString *categoryName;
@property PFFile *imageFile;
+(NSString *)parseClassName;

@end
