//
//  DataViewController.h
//  Stampitivity
//
//  Created by BHANU on 19/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryClass.h"
#import "CategoryClass.h"
#import <SDWebImage/SDWebImageManager.h>
#import "UIImageView+WebCache.h"
#import "StampitivityPayVC.h"
@interface DataViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
