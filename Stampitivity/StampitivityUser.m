//
//  StampitivityUser.m
//  Stampitivity
//
//  Created by BHANU on 17/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import "StampitivityUser.h"

@implementation StampitivityUser
@dynamic userName;
@dynamic firstName;
@dynamic lastName;
@dynamic email;
@dynamic displayName;
@dynamic password;
@dynamic location;
@dynamic userImageFile;

@synthesize friends = _friends;



+ (StampitivityUser *)currentUser {
    return (StampitivityUser *)[PFUser currentUser];
}

- (PFRelation *)friends
{
    if (!_friends) _friends = [self relationForKey:@"friends"];
    return _friends;
}


-(BOOL)isEqual:(StampitivityUser*)otherObject
{
    if ([[self objectId] isEqualToString:[otherObject objectId]])
        return YES;
    
    return NO;
}

@end
