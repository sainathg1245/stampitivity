//
//  StampitivityUser.h
//  Stampitivity
//
//  Created by BHANU on 17/08/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
@interface StampitivityUser : PFUser<PFSubclassing>
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) PFFile *userImageFile;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *Gender;
@property (nonatomic, strong) PFRelation *friends;
@property (nonatomic, strong) NSString *profileDescription;
@property NSNumber *userType;
@end
