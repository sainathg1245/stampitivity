
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#define USERNAME @"username"

@interface Utils : NSObject

+(BOOL) validateEmail:(NSString *) email;
+(BOOL) validatePassword:(NSString *) password;
+(double) getCurrentTimeinMillis;
//+(float) getAdjustedValue:(float) value;

+ (BOOL) connectedToNetwork;
@end
